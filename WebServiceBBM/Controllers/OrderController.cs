﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using WebServiceBBM;
using WebServiceBBM.Models.Order;
//using WebServiceBBM.WebServiceHNORetailSalesOrder;
//using WebServiceBBM.WebServiceHCMRetailSalesOrder;

namespace WebServiceBBM.Controllers
{
    public class OrderController : ApiController
    {
        // Creates instance of service and sets credentials.
        WebServiceHNORetailSalesOrder.RetailSalesOrder_Service serviceHNO = new WebServiceHNORetailSalesOrder.RetailSalesOrder_Service();
        WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Service serviceHCM = new WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Service();
        //serviceRSO.UseDefaultCredentials = true;        
        public OrderController()
        {
            //// Creates instance of service and sets credentials.
            //RetailSalesOrder_Service serviceRSO = new RetailSalesOrder_Service();
            //serviceRSO.UseDefaultCredentials = true;
            
        }


        
        #region get All
       /* public IHttpActionResult GetAllRSO()
        {
            serviceRSO.UseDefaultCredentials = true;
            List<RetailSalesOrder_Filter> filterArrayGet = new List<RetailSalesOrder_Filter>();
            RetailSalesOrder_Filter nameFilterGet = new RetailSalesOrder_Filter();
            nameFilterGet.Field = RetailSalesOrder_Fields.Receipt_No;
            //nameFilterGet.Criteria = id;
            filterArrayGet.Add(nameFilterGet);
            //Msg("List before modification");
            //PrintCustomerList(service, filterArrayGet);

            RetailSalesOrder[] list = serviceRSO.ReadMultiple(filterArrayGet.ToArray(), null, 10);


            return Ok(list);
        }*/
        #endregion

        // GET api/order/5
        public IHttpActionResult GetRSOById(string id)
        {
            LogBuild.CreateLogger("start get by Receipt_No", "Order");

            try
            {

                if (id == null || id.Length < 1)
                {
                    return NotFound();
                }

                string[] sub = id.Split('_');

                LogBuild.CreateLogger("Region :" + sub[0], "Order");
                LogBuild.CreateLogger("Receipt_No :" + sub[1], "Order");

                if (sub[0] == "HNO")
                {

                    //serviceHNO.UseDefaultCredentials = true;
                    serviceHNO.Credentials = new NetworkCredential("truonglx", "12345678a@A", "North.BIBOMART.LOCAL");
                    serviceHNO.PreAuthenticate = true;

                    List<WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter> filterArrayGet = new List<WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter>();
                    WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter nameFilterGet = new WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter();
                    nameFilterGet.Field = WebServiceHNORetailSalesOrder.RetailSalesOrder_Fields.Receipt_No;
                    nameFilterGet.Criteria = sub[1];
                    filterArrayGet.Add(nameFilterGet);


                    WebServiceHNORetailSalesOrder.RetailSalesOrder[] list = serviceHNO.ReadMultiple(filterArrayGet.ToArray(), null, 1);

                    if (list.Length == 0)
                    {
                        return NotFound();
                    }

                    LogBuild.CreateLogger("Return fillter :"+JsonConvert.SerializeObject(list), "Order");
                    return Ok(list);


                }
                else if (sub[0] == "HCM")
                {
                    serviceHCM.UseDefaultCredentials = true;
                    List<WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter> filterArrayGet = new List<WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter>();
                    WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter nameFilterGet = new WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter();
                    nameFilterGet.Field = WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Fields.Receipt_No;
                    nameFilterGet.Criteria = sub[1];
                    filterArrayGet.Add(nameFilterGet);


                    WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder[] list = serviceHCM.ReadMultiple(filterArrayGet.ToArray(), null, 1);

                    if (list.Length == 0)
                    {
                        return NotFound();
                    }

                    LogBuild.CreateLogger("Return fillter :" + JsonConvert.SerializeObject(list), "Order");
                    return Ok(list);
                }
                else
                {
                    return BadRequest("HNO or HCM, orther deny");
                }

            }
            catch (Exception ex)
            {
                LogBuild.CreateLogger("Error :" + JsonConvert.SerializeObject(ex), "Order");
                return BadRequest("Err" + ex.ToString());
            }
        }

        // POST api/values
        public IHttpActionResult PostRSO(OrderItem item)
        {
           

            LogBuild.CreateLogger("Create Order", "Order");

            try
            {
                LogBuild.CreateLogger("Data param : " + JsonConvert.SerializeObject(item), "Order");

                if (item.Receipt_No == null || item.Location_Code == null || item.Region==null || item.Member_Card_No == null ||
               item.Sell_to_Customer_No == null || item.Sales_Type == null || item.SalesLines.Count < 1)
                {
                    return NotFound();
                }

                if (item.Region == "HNO")
                {

                    serviceHNO.UseDefaultCredentials = true;

                    #region create  RSO info

                    WebServiceHNORetailSalesOrder.RetailSalesOrder si = new WebServiceHNORetailSalesOrder.RetailSalesOrder();

                    si.Sell_to_Customer_No = item.Sell_to_Customer_No;
                    si.Member_Card_No = item.Member_Card_No;

                    si.Receipt_No = item.Receipt_No;
                    si.Location_Code = item.Location_Code;
                    si.Sales_Type = item.Sales_Type;
                    si.Shortcut_Dimension_1_Code = item.Location_Code;

                    WebServiceHNORetailSalesOrder.Retail_Sales_Order_Line[] sl = new WebServiceHNORetailSalesOrder.Retail_Sales_Order_Line[item.SalesLines.Count];


                    for (int i = 0; i < item.SalesLines.Count; i++)
                    {
                        WebServiceHNORetailSalesOrder.Retail_Sales_Order_Line sl_sub = new WebServiceHNORetailSalesOrder.Retail_Sales_Order_Line();

                        sl_sub.Type = WebServiceHNORetailSalesOrder.Type.Item;
                        sl_sub.No = item.SalesLines[i].No;
                        sl_sub.QuantitySpecified = true;
                        sl_sub.Quantity = item.SalesLines[i].Quantity;

                        sl[i] = sl_sub;
                    }

                    si.SalesLines = sl;

                    LogBuild.CreateLogger("Sales info for create :" + JsonConvert.SerializeObject(si), "Order");
                    #endregion


                    #region create RSO on NAV

                    serviceHNO.Create(ref si);
                    Console.WriteLine(si.No);

                    #endregion

                    LogBuild.CreateLogger("Sales Order No " + si.No, "Order");
                    LogBuild.CreateLogger("Sales Order :" + JsonConvert.SerializeObject(item), "Order");

                    return Ok(si.No);
                }
                else if(item.Region=="HCM")
                {
                    serviceHCM.UseDefaultCredentials = true;

                    #region create  RSO info

                    WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder si = new WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder();

                    si.Sell_to_Customer_No = item.Sell_to_Customer_No;
                    si.Member_Card_No = item.Member_Card_No;

                    si.Receipt_No = item.Receipt_No;
                    si.Location_Code = item.Location_Code;
                    si.Sales_Type = item.Sales_Type;
                    si.Shortcut_Dimension_1_Code = item.Location_Code;

                    WebServiceHCMRetailSalesOrder.Retail_Sales_Order_Line[] sl = new WebServiceHCMRetailSalesOrder.Retail_Sales_Order_Line[item.SalesLines.Count];


                    for (int i = 0; i < item.SalesLines.Count; i++)
                    {
                        WebServiceHCMRetailSalesOrder.Retail_Sales_Order_Line sl_sub = new WebServiceHCMRetailSalesOrder.Retail_Sales_Order_Line();

                        sl_sub.Type = WebServiceHCMRetailSalesOrder.Type.Item;
                        sl_sub.No = item.SalesLines[i].No;
                        sl_sub.QuantitySpecified = true;
                        sl_sub.Quantity = item.SalesLines[i].Quantity;

                        sl[i] = sl_sub;
                    }

                    si.SalesLines = sl;

                    LogBuild.CreateLogger("Sales info for create :" + JsonConvert.SerializeObject(si), "Order");
                    #endregion


                    #region create RSO on NAV

                    serviceHCM.Create(ref si);
                    Console.WriteLine(si.No);

                    #endregion

                    LogBuild.CreateLogger("Sales Order No " + si.No, "Order");
                    LogBuild.CreateLogger("Sales Order :" + JsonConvert.SerializeObject(item), "Order");

                    return Ok(si.No);
                }
                else
                {
                    return BadRequest("HNO or HCM, orther deny");
                }
            }
            catch (Exception ex)
            {
                LogBuild.CreateLogger("Error :" + ex.ToString(), "Order");
                return BadRequest("Err" + ex.ToString());
            }
        }

        // PUT api/values/5
        #region UpdateOrder
            /*
        public IHttpActionResult PutRSO(OrderItem st)
        {
            LogBuild.CreateLogger("Update Order", "Order");
            LogBuild.CreateLogger("Data param"+st.ToString(), "Order");
            try
            {
                if (st.Region == null || st.Receipt_No==null || st.Status<1 || st.Status>3)
                {
                    return BadRequest("param lost");
                }

                serviceRSO.UseDefaultCredentials = true;
                List<RetailSalesOrder_Filter> filterArrayGet = new List<RetailSalesOrder_Filter>();
                RetailSalesOrder_Filter nameFilterGet = new RetailSalesOrder_Filter();
                nameFilterGet.Field = RetailSalesOrder_Fields.Receipt_No;
                nameFilterGet.Criteria = st.Receipt_No;
                filterArrayGet.Add(nameFilterGet);
                //Msg("List before modification");
                //PrintCustomerList(service, filterArrayGet);

                RetailSalesOrder[] list = serviceRSO.ReadMultiple(filterArrayGet.ToArray(), null, 1);
                LogBuild.CreateLogger("Filter :"+list.ToString(), "Order");

                if (list.Length == 0)
                {
                    return NotFound();
                }

                foreach (RetailSalesOrder c in list)
                {
                    serviceRSO.UseDefaultCredentials = true;
                    RetailSalesOrder_Fields.Status.Equals(st);
                    switch (st.Status)
                    {
                        case 1:
                            c.Status = WebServiceRetailSalesOrder.Status.Released;
                            break;
                        case 2:
                            c.Status = WebServiceRetailSalesOrder.Status.Pending_Approval;
                            break;
                        case 3:
                            c.Status = WebServiceRetailSalesOrder.Status.Pending_Prepayment;
                            break;
                        default:
                            c.Status = WebServiceRetailSalesOrder.Status.Released;
                            break;
                    }
                    

                    RetailSalesOrder sub = c;

                    LogBuild.CreateLogger("Sales info for update"+sub.ToString(), "Order");

                    if (st.Region == "HNO")
                    {
                        serviceRSO.Update(ref sub);
                    }
                    else
                    {
                        return BadRequest("only HNO, waiting orther");
                    }
                }


                return Ok();
            }
            catch (Exception ex)
            {
                LogBuild.CreateLogger("Error :"+ex.ToString(), "Order");
                return BadRequest(ex.ToString());
            }
        }
        */
        #endregion


        // DELETE api/values/5
        public IHttpActionResult DeleteRSO(OrderItem item)
        {
            LogBuild.CreateLogger("Cancel sales", "Order");
            try
            {
                LogBuild.CreateLogger("Item for delete" + JsonConvert.SerializeObject(item), "Order");

                if (item == null || item.Region == null || item.Receipt_No == null)
                {
                    return BadRequest("param lost");
                }



                if (item.Region == "HNO")
                {
                    serviceHNO.UseDefaultCredentials = true;
                    List<WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter> filterArrayGet = new List<WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter>();
                    WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter nameFilterGet = new WebServiceHNORetailSalesOrder.RetailSalesOrder_Filter();
                    nameFilterGet.Field = WebServiceHNORetailSalesOrder.RetailSalesOrder_Fields.Receipt_No;
                    nameFilterGet.Criteria = item.Receipt_No;
                    filterArrayGet.Add(nameFilterGet);

                    WebServiceHNORetailSalesOrder.RetailSalesOrder[] list = serviceHNO.ReadMultiple(filterArrayGet.ToArray(), null, 1);

                    LogBuild.CreateLogger("Filter :" + JsonConvert.SerializeObject(list), "Order");

                    if (list.Length == 0)
                    {
                        return NotFound();
                    }

                    LogBuild.CreateLogger("Filter :" + JsonConvert.SerializeObject(list), "Order");

                    foreach (WebServiceHNORetailSalesOrder.RetailSalesOrder c in list)
                    {
                        if (c.Status != WebServiceHNORetailSalesOrder.Status.Released)
                        {
                            c.Cancel = true;
                            //serviceRSO.Delete(c.Key);
                            WebServiceHNORetailSalesOrder.RetailSalesOrder sub = c;
                            LogBuild.CreateLogger("Sales info for cancel" + sub.ToString(), "Order");
                            serviceHNO.Update(ref sub);
                        }
                        else
                        {
                            return BadRequest("{return : Order is Released}");
                        }
                    }
                    return Ok();
                }
                else if (item.Region == "HCM")
                {
                    serviceHCM.UseDefaultCredentials = true;
                    List<WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter> filterArrayGet = new List<WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter>();
                    WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter nameFilterGet = new WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Filter();
                    nameFilterGet.Field = WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder_Fields.Receipt_No;
                    nameFilterGet.Criteria = item.Receipt_No;
                    filterArrayGet.Add(nameFilterGet);

                    WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder[] list = serviceHCM.ReadMultiple(filterArrayGet.ToArray(), null, 1);

                    LogBuild.CreateLogger("Filter :" + JsonConvert.SerializeObject(list), "Order");

                    if (list.Length == 0)
                    {
                        return NotFound();
                    }

                    LogBuild.CreateLogger("Filter :" + JsonConvert.SerializeObject(list), "Order");

                    foreach (WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder c in list)
                    {
                        if (c.Status != WebServiceHCMRetailSalesOrder.Status.Released)
                        {
                            c.Cancel = true;
                            //serviceRSO.Delete(c.Key);
                            WebServiceHCMRetailSalesOrder.HCMRetailSalesOrder sub = c;
                            LogBuild.CreateLogger("Sales info for cancel" + sub.ToString(), "Order");
                            serviceHCM.Update(ref sub);
                        }
                        else
                        {
                            return BadRequest("{return : Order is Released}");
                        }
                    }
                    return Ok();
                }
                else
                {
                    return BadRequest("HNO or HCM, orther deny");
                }
            }
            catch (Exception ex)
            {
                LogBuild.CreateLogger("Error :" + JsonConvert.SerializeObject(ex), "Order");
                return BadRequest("{ Err :" + ex.ToString() + "}");
            }
        }
    }
}
