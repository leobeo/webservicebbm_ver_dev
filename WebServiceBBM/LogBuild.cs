﻿using System;
using System.Data;
using System.Web;
using System.IO;

namespace WebServiceBBM
{
    public class LogBuild
    {
        public static void CreateLogger(string str,string namecontroller)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\" + DateTime.Now.ToString("yyyyMMdd") + "_Log"+namecontroller+".txt", true);
                sw.WriteLine(DateTime.Now.ToString("g") + ": " + str);
                sw.Flush();
                sw.Close();
            }
            catch
            {
                // ignored
            }
        }
    }
}