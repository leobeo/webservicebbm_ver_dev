﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceBBM.Models.Order
{
    public class OrderItem
    {
        public string ID { set; get; }
        public string Receipt_No { set; get; }
        public string Sell_to_Customer_No { set; get; }
        public string Member_Card_No { set; get; }
        public int Status { set; get; }
        public string Region { set; get; }
        public string Location_Code { set; get; }
        public string Sales_Type { set; get; }
        public List<OrderProductItem> SalesLines { set; get; }
    }
}

