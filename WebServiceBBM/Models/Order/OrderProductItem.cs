﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebServiceBBM.Models.Order
{
    public class OrderProductItem
    {
        public string Id { set; get; }
        public string No { get; set; }
        public int Quantity { set; get; }
    }
}
